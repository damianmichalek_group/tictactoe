﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static TicTacToe.MiniMaxLogic;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const int GridSize = 4;
        public readonly Button[,] tiles = new Button[GridSize, GridSize];
        public bool isPlayer1Turn = true;
        public int turnCount = 0;
        public int maxTurns = GridSize * GridSize;
        public int winSequence = 4;
        public static MainWindow? self;
        char[,] currentBoard;

        public MainWindow()
        {
            InitializeComponent();
            InitializeGameGrid();
            PlaceGameButtons();
            self = this;
        }

        public void InitializeGameGrid()
        {
            for (int i = 0; i < GridSize; i++)
            {
                GameGrid.RowDefinitions.Add(new RowDefinition());
                GameGrid.RowDefinitions[i].Height = new GridLength(1, GridUnitType.Star);
                GameGrid.ColumnDefinitions.Add(new ColumnDefinition());
                GameGrid.ColumnDefinitions[i].Width = new GridLength(1, GridUnitType.Star);
            }
        }

        public void PlaceGameButtons()
        {
            turnCount = 0;
            isPlayer1Turn = true;

            for (int row = 0; row < GridSize; row++)
            {
                for (int col = 0; col < GridSize; col++)
                {
                    Button button = new Button
                    {
                        Background = new SolidColorBrush(Colors.Silver),
                        Margin = new Thickness(5)
                    };

                    button.Click += OnButtonClick;
                    button.FontSize = 50;
                    button.FontWeight = FontWeights.Bold;

                    GameGrid.Children.Add(button);
                    Grid.SetRow(button, row);
                    Grid.SetColumn(button, col);

                    tiles[row, col] = button;
                }
            }
        }

        public void OnButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;

            button.Content = "X";
            button.Foreground = new SolidColorBrush(Colors.DarkBlue);
            button.IsEnabled = false;
            turnCount++;

            GameState.CheckGameState();

            if (!GameState.CheckForDraw())
            {
                AIMakeMove();
            }
        }

        //setnu aby byl best move v current board
        //convert na button
        //Disable button
        //add symbol
        public void AIMakeMove()
        {

            // vezmu si aktualni stav tiles
            // prevedu tiles na board
            currentBoard = new MiniMaxLogic().FromButtonsToBoard(tiles);

            // zavolam minimax
            // dostanu bestMove
            Move bestMove = MiniMaxLogic.findBestMove(currentBoard);

            // vezmu si button na bestMove.row, bestMove.col
            Button currentButton = tiles[bestMove.row, bestMove.col];

            // "kliknu" na nej
            currentButton.Content = "O";
            currentButton.Foreground = new SolidColorBrush(Colors.DarkRed);
            // zmenim stav tlacitka na disabled nebo takneco
            currentButton.IsEnabled = false;

            // zavolam GameState.CheckGameState();
            GameState.CheckGameState();
        }

        public void RestartButton_Click(object sender, RoutedEventArgs e)
        {
            DeleteGameGrid();
            InitializeGameGrid();
            PlaceGameButtons();
            EndGameGrid.Visibility = Visibility.Collapsed;
        }

        public void DeleteGameGrid()
        {
            GameGrid.Children.Clear();
            GameGrid.RowDefinitions.Clear();
            GameGrid.ColumnDefinitions.Clear();
        }

        public void ShowEndGameMessage(string message)
        {
            EndGameMessage.Text = message;
            EndGameGrid.Visibility = Visibility.Visible;
        }
    }
}
