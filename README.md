# Tic tac toe with minimax algorithm

## "important" variables locations:

### MainWindow.xaml.cs
- int GridSize - used to set the size of the square grid
- int winSequence - used to set the number of consecutive symbols needed to win

### MiniMaxLogic.cs
- int maxDepth - used to set the maximum depth of the minimax algorithm

## Issues that come to mind:
- Extremely slow for grid sizes larger than 4x4
- Good luck trying to play 10x10 with high enough maxDepth
- But it should work even on big fields, it just takes exponentionally longer each time
- Pruining is commented out because it seemed to mess with picking the best move

## Recommended board settings:
- i was able to get a 4x4 grid to work with a depth of 6, but at that point it was pretty slow and also not that intelligent, since it coudlnt calculate that many moves
- 3x3 grid seems to work well even with unlimited depth I believe