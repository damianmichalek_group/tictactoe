﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    internal static class GameState
    {
        public static void CheckGameState()
        {
            if (CheckForWin())
            {
                if (MainWindow.self.isPlayer1Turn)
                {
                    MainWindow.self.ShowEndGameMessage("Player 1 wins!");
                }
                else
                {
                    MainWindow.self.ShowEndGameMessage("Player 2 wins!");
                }
            }

            else if (CheckForDraw()) // no winner + board full => draw
            {
                MainWindow.self.ShowEndGameMessage("It's a draw!");
            }

            else
            {
                SwitchPlayers();
            }
        }

        public static void SwitchPlayers()
        {
            MainWindow.self.isPlayer1Turn = !MainWindow.self.isPlayer1Turn;
        }

        public static bool CheckForWin()
        // asked gpt only to help with logic using pseudocode in this method,
        // because i couldn't entirely figure out checking for smaller win sequences on bigger grid
        // gpt didn't provide any code, only helped with logic
        {
            // Check rows
            for (int i = 0; i < MainWindow.GridSize - 1; i++)
            {
                for (int j = 0; j <= MainWindow.GridSize - MainWindow.self.winSequence; j++)
                {

                    if (MainWindow.self.tiles[i, j].Content != null)
                    {
                        bool isWin = true;
                        for (int k = 1; k < MainWindow.self.winSequence; k++)
                        {
                            if (MainWindow.self.tiles[i, j + k].Content == null || MainWindow.self.tiles[i, j + k].Content != MainWindow.self.tiles[i, j].Content)
                            {
                                isWin = false;
                                break;
                            }
                        }

                        if (isWin)
                        {
                            return true;
                        }
                    }
                }
            }

            // Check columns
            for (int i = 0; i <= MainWindow.GridSize - MainWindow.self.winSequence; i++)
            {
                for (int j = 0; j < MainWindow.GridSize - 1; j++)
                {
                    if (MainWindow.self.tiles[i, j].Content != null)
                    {
                        bool isWin = true;
                        for (int k = 1; k < MainWindow.self.winSequence; k++)
                        {
                            if (MainWindow.self.tiles[i + k, j].Content == null || MainWindow.self.tiles[i + k, j].Content != MainWindow.self.tiles[i, j].Content)
                            {
                                isWin = false;
                                break;
                            }
                        }

                        if (isWin)
                        {
                            return true;
                        }
                    }
                }
            }

            // Check diagonals
            for (int i = 0; i <= MainWindow.GridSize - MainWindow.self.winSequence; i++)
            {
                for (int j = 0; j <= MainWindow.GridSize - MainWindow.self.winSequence; j++)
                {
                    if (MainWindow.self.tiles[i, j].Content != null)
                    {
                        bool isWin = true;
                        for (int k = 1; k < MainWindow.self.winSequence; k++)
                        {
                            if (MainWindow.self.tiles[i + k, j + k].Content == null || MainWindow.self.tiles[i + k, j + k].Content != MainWindow.self.tiles[i, j].Content)
                            {
                                isWin = false;
                                break;
                            }
                        }

                        if (isWin)
                        {
                            return true;
                        }
                    }
                }
            }

            // Check anti-diagonals
            for (int i = 0; i <= MainWindow.GridSize - MainWindow.self.winSequence; i++)
            {
                for (int j = MainWindow.self.winSequence - 1; j < MainWindow.GridSize; j++)
                {
                    if (MainWindow.self.tiles[i, j].Content != null)
                    {
                        bool isWin = true;
                        for (int k = 1; k < MainWindow.self.winSequence; k++)
                        {
                            if (MainWindow.self.tiles[i + k, j - k].Content == null || MainWindow.self.tiles[i + k, j - k].Content != MainWindow.self.tiles[i, j].Content)
                            {
                                isWin = false;
                                break;
                            }
                        }

                        if (isWin)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public static bool CheckForDraw()
        {
            if (!CheckForWin() && MainWindow.self.turnCount >= MainWindow.self.maxTurns) // no winner + board full => draw
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}